pipeline{
    agent {
        label 'jenkins_slave'
    }
    tools {
        maven 'maven-auto'
        jdk 'jdk21'
    }
    environment{
        workspace="/data/"
    }
    stages{
        stage("limpiar"){
            steps{
                cleanWs()
            }
        }
        stage("Descargar el proyecto"){
            steps{
                git credentialsId: 'git-credentials', branch: "develop", url: "https://gitlab.com/kevil/restful-web-service-with-springbot.git"
                echo "Proyecto descargado"
            }
        }
        stage("Realizar el Build"){
            steps{
                sh "chmod -R +rx /opt/jdk21"
                echo "Iniciando el Build"
                sh "mvn -v"
                sh "pwd"
                sh "mvn clean compile package -Dmaven.test.skip=true -U"
                sh "pwd"
                sh "mv target/*.jar target/app.jar"
                stash includes: "target/app.jar", name: 'backartifact'
                archiveArtifacts artifacts: "target/app.jar", onlyIfSuccessful:true
                sh "cp target/app.jar /tmp/"
            }
        }
        stage("Test de Vulnerabilidad"){
            steps{
                sh "/grype /tmp/app.jar  > informe-scan.txt"
                sh "pwd"
                archiveArtifacts artifacts: "informe-scan.txt", onlyIfSuccessful:true
            }
        }
        stage('sonarqube analysis'){
            steps{
               script{
                   sh "pwd"
						writeFile encoding: 'UTF-8', file: 'sonar-project.properties', text: """sonar.projectKey=sonar_key
						sonar.projectName=academy
						sonar.projectVersion=academy
						sonar.sourceEncoding=UTF-8
						sonar.sources=src/main/
						sonar.java.binaries=target/
						sonar.java.libraries=target/classes
						sonar.language=java
						sonar.scm.provider=git
						"""
                        // Sonar Disabled due to we don't have a sonar in tools account yet
						withSonarQubeEnv('Sonar_CI') {
						     def scannerHome = tool 'Sonar_CI'
						     sh "${tool("Sonar_CI")}/bin/sonar-scanner -X"
						}
               }
        
            }
        }
    }
}