insert into gestion (deleted,yearG,id,num,uuid)
values (false,2022,1000,1,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e0f');

insert into gestion (deleted,yearG,id,num,uuid)
values (false,2022,1001,2,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e0g');

insert into gestion (deleted,yearG,id,num,uuid)
values (false,2023,1002,1,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e0h');

insert into faculty (code,name,gestion_id,uuid,id)
values ('FCYT','Facultad de Ciencias y Tecnologia',1000,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e01',2000);
insert into faculty (code,name,gestion_id,uuid,id)
values ('FCE','Facultad de Ciencias Economicas',1000,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e02',2001);
insert into faculty (code,name,gestion_id,uuid,id)
values ('FM','Facultad de Medicina',1001,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e03',2002);

insert into career (code,name,faculty_id,uuid,id)
values ('INF','Ingenieria Informatica',2000,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e04',2000);
insert into career (code,name,faculty_id,uuid,id)
values ('SIS','Ingenieria en Sistemas',2000,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e05',2001);
insert into career (code,name,faculty_id,uuid,id)
values ('BIO','BIOQUIMICA',2000,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e06',2002);

insert into subject (code,name,group_subject,career_id,uuid,id)
values ('IN','INGLES I',1,2000,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e10',3000);
insert into subject (code,name,group_subject,career_id,uuid,id)
values ('ELEM','ELEMENTOS',2,2000,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e11',3001);
insert into subject (code,name,group_subject,career_id,uuid,id)
values ('IN','INGLES I',2,2000,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e12',3002);

insert into student (code_sys,name,qualification,status,subject_id,uuid,id)
values ('201822233','Marco Lopex',90,'Approve',3000,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e50',4000);
insert into student (code_sys,name,qualification,status,subject_id,uuid,id)
values ('201722233','Juana Mirian',52,'Approve',3000,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e51',4001);
insert into student (code_sys,name,qualification,status,subject_id,uuid,id)
values ('201622233','Jimmi Koler',80,'Approve',3000,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e52',4002);

