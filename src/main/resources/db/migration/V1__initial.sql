-- create gestion table
create table gestion (
                         id integer not null,
                         num integer not null,
                         yearg integer not null,
                         uuid character varying(36) NOT NULL,
                         deleted boolean NOT NULL DEFAULT false,
                         primary key (id)
);

-- create a sequence for gestion id
create sequence gestion_sequence as integer increment 1;

-- create faculty table
create table faculty (
                        id integer not null,
                        uuid character varying(36) NOT NULL,
                        code character varying(20),
                        name character varying(500),
                        gestion_id integer NOT NULL,
                        deleted boolean NOT NULL DEFAULT false,
                        primary key (id)
);
-- create a sequence for faculty id
create sequence faculty_sequence as integer increment 1;

-- create career table
create table career (
                        id integer not null,
                        uuid character varying(36) NOT NULL,
                        code character varying(20),
                        name character varying(500),
                        faculty_id integer NOT NULL,
                        deleted boolean NOT NULL DEFAULT false,
                        primary key (id)
);
-- create a sequence for career id
create sequence career_sequence as integer increment 1;

-- create subject table
create table subject (
                        id integer not null,
                        uuid character varying(36) NOT NULL,
                        code character varying(20),
                        name character varying(500),
                        group_subject integer,
                        career_id integer NOT NULL,
                        deleted boolean NOT NULL DEFAULT false,
                        primary key (id)
);
-- create a sequence for subject id
create sequence subject_sequence as integer increment 1;

-- create student table
create table student (
                        id integer not null,
                        uuid character varying(36) NOT NULL,
                        code_sys character varying(20),
                        name character varying(500),
                        status character varying(30),
                        qualification integer,
                        subject_id integer NOT NULL,
                        deleted boolean NOT NULL DEFAULT false,
                        primary key (id)
);
-- create a sequence for student id
create sequence student_sequence as integer increment 1;


alter table faculty add constraint fk_faculty_gestion foreign key (gestion_id)
    references gestion (id) on delete restrict on update restrict;

alter table career add constraint fk_career_faculty foreign key (faculty_id)
    references faculty (id) on delete restrict on update restrict;

alter table subject add constraint fk_subject_career foreign key (career_id)
    references career (id) on delete restrict on update restrict;

alter table student add constraint fk_student_subject foreign key (subject_id)
    references subject (id) on delete restrict on update restrict;

