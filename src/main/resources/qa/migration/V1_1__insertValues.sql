insert into gestion (deleted,yearG,id,num,uuid)
values (false,2025,1000,2,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e0f');
insert into gestion (deleted,yearG,id,num,uuid)
values (false,2022,1001,2,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e0g');

insert into faculty (code,name,gestion_id,uuid,id)
values ('FH','Humanidades',1000,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e13',2000);
insert into faculty (code,name,gestion_id,uuid,id)
values ('FH','Humanidades',1001,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e14',2001);

insert into career (code,name,faculty_id,uuid,id)
values ('II','Ingenieria Informatica',2000,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e04',2000);
insert into career (code,name,faculty_id,uuid,id)
values ('Psi','Psicologia',2001,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e05',2001);

insert into subject (code,name,group_subject,career_id,uuid,id)
values ('I','Intro',1,2000,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e10',3000);
insert into subject (code,name,group_subject,career_id,uuid,id)
values ('Fr.','Frances',1,2001,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e11',3001);

insert into student (code_sys,name,qualification,status,subject_id,uuid,id)
values ('201822233','Marco Lopex',90,'Approve',3001,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e50',4000);
insert into student (code_sys,name,qualification,status,subject_id,uuid,id)
values ('201722233','Juana Mirian',52,'Approve',3001,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e51',4001);
insert into student (code_sys,name,qualification,status,subject_id,uuid,id)
values ('201622233','Jimmi Koler',80,'Approve',3001,'b79a2b99-0fdb-4dbd-83d6-7d6a76c52e52',4002);


