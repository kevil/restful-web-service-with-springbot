package com.dedae.restfulwebservice.dto;


public class FacultyDTO {
    private String name;
    private String code;
    private String uuid;
    private GestionDTO gestion;
    public FacultyDTO() {
    }

    public FacultyDTO(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public GestionDTO getGestionDTO() {
        return gestion;
    }

    public void setGestionDTO(GestionDTO gestionDTO) {
        this.gestion = gestionDTO;
    }
}
