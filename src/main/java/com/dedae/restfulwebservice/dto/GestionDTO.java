package com.dedae.restfulwebservice.dto;

public class GestionDTO {
    private String Uuid;
    private Integer num;
    private Integer yearG;

    public GestionDTO(String uuid, Integer num, Integer yearG) {
        Uuid = uuid;
        this.num = num;
        this.yearG = yearG;
    }

    public GestionDTO() {
    }

    public String getUuid() {
        return Uuid;
    }

    public void setUuid(String uuid) {
        Uuid = uuid;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getYear() {
        return yearG;
    }

    public void setYear(Integer yearG) {
        this.yearG = yearG;
    }
}
