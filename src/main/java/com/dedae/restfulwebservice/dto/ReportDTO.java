package com.dedae.restfulwebservice.dto;

public class ReportDTO {
    private String name;
    private float qualification;

    public ReportDTO() {
    }

    public ReportDTO(String name, float qualification) {
        this.name = name;
        this.qualification = qualification;
    }

    public String getName() {
        return name;
    }

    public float getQualification() {
        return qualification;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setQualification(float qualification) {
        this.qualification = qualification;
    }
}
