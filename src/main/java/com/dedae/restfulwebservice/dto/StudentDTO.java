package com.dedae.restfulwebservice.dto;

import lombok.AllArgsConstructor;


public class StudentDTO {
    private String name;
    private String code_sys;
    private Integer qualification;
    private String status;
    private String uuid;
    private SubjectDTO subject;

    public StudentDTO() {
    }

    public StudentDTO(String name, String code_sys, Integer qualification, String status) {
        this.name = name;
        this.code_sys = code_sys;
        this.qualification = qualification;
        this.status = status;
    }

    public StudentDTO(String name, String codeSys, int qualification, String status, String uuid) {
        this.name = name;
        this.code_sys = codeSys;
        this.qualification = qualification;
        this.status = status;
        this.uuid=uuid;
    }

    public StudentDTO(String name) {
        this.name=name;
    }

    public StudentDTO(String name, String codeSys) {
        this.name = name;
        this.code_sys = codeSys;
    }

    public StudentDTO(String name, String codeSys, int qualification) {
        this.name = name;
        this.code_sys = codeSys;
        this.qualification=qualification;
    }

    public StudentDTO(String name, int qualification) {
        this.name = name;
        this.qualification=qualification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode_sys() {
        return code_sys;
    }

    public void setCode_sys(String code_sys) {
        this.code_sys = code_sys;
    }

    public Integer getQualification() {
        return qualification;
    }

    public void setQualification(Integer qualification) {
        this.qualification = qualification;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public SubjectDTO getSubject() {
        return subject;
    }

    public void setSubject(SubjectDTO subject) {
        this.subject = subject;
    }
}
