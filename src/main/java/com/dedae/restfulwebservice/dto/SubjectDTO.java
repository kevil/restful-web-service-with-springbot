package com.dedae.restfulwebservice.dto;

public class SubjectDTO {
    private String name;
    private String code;
    private Integer group;
    private String uuid;
    private CareerDTO career;

    public SubjectDTO() {
    }

    public SubjectDTO(String name, String code, Integer group) {
        this.name = name;
        this.code = code;
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public CareerDTO getCareer() {
        return career;
    }

    public void setCareer(CareerDTO career) {
        this.career = career;
    }
}
