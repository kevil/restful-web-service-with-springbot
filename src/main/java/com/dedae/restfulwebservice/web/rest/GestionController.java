package com.dedae.restfulwebservice.web.rest;

import com.dedae.restfulwebservice.dto.FacultyDTO;
import com.dedae.restfulwebservice.dto.GestionDTO;
import com.dedae.restfulwebservice.dto.ReportDTO;
import com.dedae.restfulwebservice.dto.StudentDTO;
import com.dedae.restfulwebservice.services.implement.GestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/gestions")
public class GestionController {
    @Autowired
    GestionService gestionService;
    @GetMapping
    public List<GestionDTO> getAll(){
        return gestionService.getAllGestions();
    }
    @GetMapping("/{gestionUuid}")
    public GestionDTO get(@PathVariable String gestionUuid){
        return gestionService.getGestion(gestionUuid);
    }

    @PostMapping
    public ResponseEntity<GestionDTO> create(@RequestBody GestionDTO gestion) throws URISyntaxException {
        GestionDTO gestionDTO = gestionService.saveGestion(gestion);
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{gestionUuid}")
                .buildAndExpand(gestionDTO.getUuid())
                .toUri();
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header(HttpHeaders.LOCATION, uri.toString())
                .body(gestionDTO);
    }

    @PutMapping("/{gestionUuid}")
    public GestionDTO update(@RequestBody GestionDTO gestion){
        return gestionService.updateGestion(gestion);
    }
    @DeleteMapping("/{gestionUuid}")
    public GestionDTO delete(@PathVariable String gestionUuid){
        return gestionService.deleteGestion(gestionUuid);
    }

    @GetMapping("/{gestionUuid}/faculties")
    public List<FacultyDTO> getFaculties(@PathVariable String gestionUuid){
        return this.gestionService.getFaculties(gestionUuid);
    }
    @GetMapping("/{gestionUuid}/students")
    public List<StudentDTO> getStudents(@PathVariable String gestionUuid){
        return this.gestionService.getStudents(gestionUuid);
    }
    @GetMapping("/{gestionUuid}/report")
    public List<ReportDTO> getReport(@PathVariable String gestionUuid){
        return this.gestionService.getReport(gestionUuid);
    }
}
