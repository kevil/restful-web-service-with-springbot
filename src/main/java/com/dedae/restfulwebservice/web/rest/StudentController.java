package com.dedae.restfulwebservice.web.rest;

import com.dedae.restfulwebservice.dto.StudentDTO;
import com.dedae.restfulwebservice.services.implement.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/students")
public class StudentController {
    @Autowired
    private StudentService studentService;
    @GetMapping("/{studentUuid}")
    public StudentDTO get(@PathVariable String studentUuid){
        return studentService.getStudent(studentUuid);
    }

    @PostMapping
    public List<StudentDTO> create(@RequestBody List<StudentDTO> studentListDTO){
        return studentService.saveStudent(studentListDTO);
    }
    @PutMapping("/{studentUuid}")
    public StudentDTO update(@PathVariable String studentUuid, @RequestBody StudentDTO studentDTO){
        return studentService.updateStudent(studentDTO);
    }
    @DeleteMapping("/{studentUuid}")
    public ResponseEntity<Void> delete(){
        studentService.deleteStudent();
        return ResponseEntity.noContent().build();
    }
}
