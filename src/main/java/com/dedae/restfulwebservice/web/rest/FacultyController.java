package com.dedae.restfulwebservice.web.rest;

import com.dedae.restfulwebservice.dto.CareerDTO;
import com.dedae.restfulwebservice.dto.FacultyDTO;
import com.dedae.restfulwebservice.services.implement.FacultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/faculties")
public class FacultyController {
    @Autowired
    private FacultyService facultyService;
    @GetMapping("/{facultyUuid}")
    public FacultyDTO get(@PathVariable String facultyUuid){
        return facultyService.getFaculty(facultyUuid);
    }

    @PostMapping
    public FacultyDTO create(@RequestBody FacultyDTO facultyDTO){
        return facultyService.saveFaculty(facultyDTO);
    }
    @PutMapping("/{facultyUuid}")
    public FacultyDTO update(@PathVariable String facultyUuid, @RequestBody FacultyDTO facultyDTO){
        return facultyService.updateFaculty(facultyDTO);
    }
    @DeleteMapping("/{facultyUuid}")
    public FacultyDTO delete(@PathVariable String facultyUuid){
        return facultyService.deleteFaculty(facultyUuid);
    }
    @GetMapping("/{facultyUuid}/careers")
    public List<CareerDTO> getCareers(@PathVariable String facultyUuid){
        return this.facultyService.getCareers(facultyUuid);
    }
}
