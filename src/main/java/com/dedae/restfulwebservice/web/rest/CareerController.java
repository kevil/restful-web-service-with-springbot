package com.dedae.restfulwebservice.web.rest;

import com.dedae.restfulwebservice.dto.CareerDTO;
import com.dedae.restfulwebservice.dto.SubjectDTO;
import com.dedae.restfulwebservice.services.implement.CareerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;


@RestController
@RequestMapping("/careers")
public class CareerController {
    @Autowired
    private CareerService careerService;
    @GetMapping("/{careerUuid}")
    public CareerDTO get(@PathVariable String careerUuid){
        return careerService.getCareer(careerUuid);
    }

    @PostMapping
    public CareerDTO create(@RequestBody CareerDTO careerDTO){
        return careerService.saveCareer(careerDTO);
    }
    @PutMapping("/{careerUuid}")
    public CareerDTO update(@PathVariable String careerUuid, @RequestBody CareerDTO careerDTO){
        return careerService.updateCareer(careerDTO);
    }
    @DeleteMapping("/{careerUuid}")
    public CareerDTO delete(@PathVariable String careerUuid){
        return careerService.deleteCareer(careerUuid);
    }

    @GetMapping("/{careerUuid}/subjects")
    public List<SubjectDTO> getCareers(@PathVariable String careerUuid){
        return this.careerService.getSubjects(careerUuid);
    }
}
