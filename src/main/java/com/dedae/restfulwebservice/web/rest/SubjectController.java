package com.dedae.restfulwebservice.web.rest;

import com.dedae.restfulwebservice.dto.StudentDTO;
import com.dedae.restfulwebservice.dto.SubjectDTO;
import com.dedae.restfulwebservice.services.implement.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/subjects")
public class SubjectController {
    @Autowired
    private SubjectService subjectService;
    @GetMapping("/{subjectUuid}")
    public SubjectDTO get(@PathVariable String subjectUuid){
        return subjectService.getSubject(subjectUuid);
    }

    @PostMapping
    public SubjectDTO create(@RequestBody SubjectDTO subjectDTO){
        return subjectService.saveSubject(subjectDTO);
    }
    @PutMapping("/{subjectUuid}")
    public SubjectDTO update(@PathVariable String subjectUuid, @RequestBody SubjectDTO subjectDTO){
        return subjectService.updateSubject(subjectDTO);
    }
    @DeleteMapping("/{subjectUuid}")
    public SubjectDTO delete(@PathVariable String subjectUuid){
        return subjectService.deleteSubject(subjectUuid);
    }

    @GetMapping("/{subjectUuid}/students")
    public List<StudentDTO> getStudents(@PathVariable String subjectUuid){
        return this.subjectService.getStudents(subjectUuid);
    }
    @DeleteMapping("/{subjectUuid}/students")
    public ResponseEntity<Void> deleteStudents(@PathVariable String subjectUuid){
        subjectService.deleteStudents(subjectUuid);
        return ResponseEntity.noContent().build();
    }

}
