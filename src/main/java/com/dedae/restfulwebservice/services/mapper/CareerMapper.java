package com.dedae.restfulwebservice.services.mapper;

import com.dedae.restfulwebservice.domain.entities.Career;
import com.dedae.restfulwebservice.domain.entities.Faculty;
import com.dedae.restfulwebservice.dto.CareerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



@Component
public class CareerMapper {
    @Autowired
    private FacultyMapper facultyMapper;
        public CareerDTO toDTO(Career career, boolean mapFaculty){
        CareerDTO careerDTO = new CareerDTO();
        careerDTO.setUuid(career.getUuid());
        careerDTO.setName(career.getName());
        careerDTO.setCode(career.getCode());
        if (mapFaculty){
            careerDTO.setFaculty(facultyMapper.toDTO(career.getFaculty(), true));
        }
        return careerDTO;
    }

    public Career getCareer(CareerDTO careerDTO, Faculty faculty){
        Career career = new Career();
        career.setUuid(careerDTO.getUuid());
        career.setName(careerDTO.getName());
        career.setCode(careerDTO.getCode());
        career.setFaculty(faculty);
        return career;
    }
}
