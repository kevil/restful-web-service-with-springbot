package com.dedae.restfulwebservice.services.mapper;

import com.dedae.restfulwebservice.domain.entities.Faculty;
import com.dedae.restfulwebservice.domain.entities.Gestion;
import com.dedae.restfulwebservice.dto.FacultyDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class FacultyMapper {
    @Autowired
    private GestionMapper gestionMapper;
    public FacultyDTO toDTO(Faculty faculty, boolean mapGestion){
        FacultyDTO facultyDTO = new FacultyDTO();
        facultyDTO.setUuid(faculty.getUuid());
        facultyDTO.setName(faculty.getName());
        facultyDTO.setCode(faculty.getCode());
        if (mapGestion){
            facultyDTO.setGestionDTO(gestionMapper.toDTO(faculty.getGestion()));
        }

        return facultyDTO;
    }

    public Faculty getFaculty(FacultyDTO facultyDTO, Gestion gestion){
        Faculty faculty = new Faculty();
        faculty.setUuid(facultyDTO.getUuid());
        faculty.setName(facultyDTO.getName());
        faculty.setCode(facultyDTO.getCode());
        faculty.setGestion(gestion);
        return faculty;
    }
}
