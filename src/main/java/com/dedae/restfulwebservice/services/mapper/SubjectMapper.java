package com.dedae.restfulwebservice.services.mapper;

import com.dedae.restfulwebservice.domain.entities.Career;
import com.dedae.restfulwebservice.domain.entities.Subject;
import com.dedae.restfulwebservice.dto.SubjectDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class SubjectMapper {
    @Autowired
    private CareerMapper careerMapper;
    public SubjectDTO toDTO(Subject subject, boolean mapCareer){
        SubjectDTO subjectDTO = new SubjectDTO();
        subjectDTO.setUuid(subject.getUuid());
        subjectDTO.setName(subject.getName());
        subjectDTO.setCode(subject.getCode());
        subjectDTO.setGroup(subject.getGroup_subject());
        if (mapCareer){
            subjectDTO.setCareer(careerMapper.toDTO(subject.getCareer(), true));
        }
        return subjectDTO;
    }

    public Subject getSubject(SubjectDTO subjectDTO, Career career){
        Subject subject = new Subject();
        subject.setUuid(subjectDTO.getUuid());
        subject.setName(subjectDTO.getName());
        subject.setCode(subjectDTO.getCode());
        subject.setGroup_subject(subjectDTO.getGroup());
        subject.setCareer(career);
        return subject;
    }
}
