package com.dedae.restfulwebservice.services.mapper;

import com.dedae.restfulwebservice.domain.entities.Gestion;
import com.dedae.restfulwebservice.dto.GestionDTO;
import org.springframework.stereotype.Component;


@Component
public class GestionMapper {
    public GestionDTO toDTO(Gestion gestion){
        GestionDTO gestionDTO = new GestionDTO();
        gestionDTO.setUuid(gestion.getUuid());
        gestionDTO.setNum(gestion.getNum());
        gestionDTO.setYear(gestion.getYear());
        return gestionDTO;
    }

    public Gestion getGestion(GestionDTO gestionDTO){
        Gestion gestion = new Gestion();
        gestion.setUuid(gestionDTO.getUuid());
        gestion.setNum(gestionDTO.getNum());
        gestion.setYear(gestionDTO.getYear());
        return gestion;
    }
}
