package com.dedae.restfulwebservice.services.mapper;

import com.dedae.restfulwebservice.domain.entities.Student;
import com.dedae.restfulwebservice.domain.entities.Subject;
import com.dedae.restfulwebservice.dto.StudentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StudentMapper {
    @Autowired
    private SubjectMapper subjectMapper;
    public StudentDTO toDTO(Student student, boolean mapSubject){
        StudentDTO studentDTO = new StudentDTO();
        studentDTO.setUuid(student.getUuid());
        studentDTO.setName(student.getName());
        studentDTO.setCode_sys(student.getCode_sys());
        studentDTO.setQualification(student.getQualification());
        studentDTO.setStatus(student.getStatus());
        if (mapSubject){
            studentDTO.setSubject(subjectMapper.toDTO(student.getSubject(), true));
        }
        return studentDTO;
    }

    public Student getStudent(StudentDTO studentDTO, Subject subject){
        Student student = new Student();
        student.setUuid(studentDTO.getUuid());
        student.setName(studentDTO.getName());
        student.setCode_sys(studentDTO.getCode_sys());
        student.setQualification(studentDTO.getQualification());
        student.setStatus(studentDTO.getStatus());
        student.setSubject(subject);
        return student;
    }
}
