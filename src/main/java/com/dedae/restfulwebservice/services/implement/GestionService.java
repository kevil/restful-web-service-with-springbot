package com.dedae.restfulwebservice.services.implement;

import com.dedae.restfulwebservice.domain.entities.Faculty;
import com.dedae.restfulwebservice.domain.entities.Gestion;
import com.dedae.restfulwebservice.dto.FacultyDTO;
import com.dedae.restfulwebservice.dto.GestionDTO;
import com.dedae.restfulwebservice.dto.ReportDTO;
import com.dedae.restfulwebservice.dto.StudentDTO;
import com.dedae.restfulwebservice.exceptions.NotFoundException;
import com.dedae.restfulwebservice.repositories.data.FacultyRepository;
import com.dedae.restfulwebservice.repositories.data.GestionRepository;
import com.dedae.restfulwebservice.repositories.jdbc.StudentJdbcRepository;
import com.dedae.restfulwebservice.services.mapper.FacultyMapper;
import com.dedae.restfulwebservice.services.mapper.GestionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class GestionService {
    @Autowired
    GestionRepository gestionRepository;
    @Autowired
    GestionMapper gestionMapper;
    @Autowired
    FacultyMapper facultyMapper;
    @Autowired
    FacultyRepository facultyRepository;

    @Autowired StudentJdbcRepository studentJdbcRepository;


    public List<GestionDTO> getAllGestions() {
        return gestionRepository
                .findAll()
                .stream()
                .map(university -> gestionMapper.toDTO(university))
                .collect(Collectors.toList());
    }

    public GestionDTO saveGestion(GestionDTO gestionDTO) {
        Gestion gestion = gestionMapper.getGestion(gestionDTO);
        return gestionMapper.toDTO(gestionRepository.save(gestion));
    }

    public GestionDTO getGestion(String gestionUuid) {
        Gestion gestion = gestionRepository.findByUuid(gestionUuid);
        if(gestion==null){
            throw new NotFoundException("Gestion",gestionUuid);
        }
        return gestionMapper.toDTO(gestion);
    }

    public GestionDTO deleteGestion(String gestionUuid) {
        Gestion gestionExample1 = new Gestion(gestionUuid);
        Optional<Gestion> optionalGestion = gestionRepository.findOne(Example.of(gestionExample1));

        if(optionalGestion.isEmpty()){
            throw new NotFoundException("Gestion", gestionUuid);
        }
        gestionRepository.delete(optionalGestion.get());
        return gestionMapper.toDTO(optionalGestion.get());
    }

    public GestionDTO updateGestion(GestionDTO gestionDTO) {
        Gestion gestionExample1 = new Gestion(gestionDTO.getUuid());
        Optional<Gestion> optionalGestion = gestionRepository.findOne(Example.of(gestionExample1));

        if(optionalGestion.isEmpty()){
            throw new NotFoundException("Gestion", gestionDTO.getUuid());
        }
        Gestion gestion2 = gestionRepository.findByUuid(gestionDTO.getUuid());
        gestion2.setYear(gestionDTO.getYear());
        gestion2.setNum(gestionDTO.getNum());

        Gestion gestion = optionalGestion.get();
        gestion.setYear(gestionDTO.getYear());
        gestion.setNum(gestionDTO.getNum());

        gestionRepository.save(gestion2);
        return gestionMapper.toDTO(gestion2);
    }

    public List<FacultyDTO> getFaculties(String gestionUuid) {
        Gestion gestionSearch = new Gestion();
        gestionSearch.setUuid(gestionUuid);
        Optional<Gestion> optionalGestion = gestionRepository.findOne(Example.of(gestionSearch));

        if(optionalGestion.isEmpty()){
            throw new NotFoundException("Gestion", gestionUuid);
        }
        Gestion gestion = optionalGestion.get();
        Gestion gestionExample = new Gestion();
        gestionExample.setId(gestion.getId());

        Faculty example = new Faculty();
        example.setGestion(gestionExample);

        List<Faculty> faculties = facultyRepository.findAll(Example.of(example));
        return faculties
                .stream()
                .map(faculty -> facultyMapper.toDTO(faculty, false))
                .collect(Collectors.toList());
    }

    public List<StudentDTO> getStudents(String gestionUuid) {
        return studentJdbcRepository.quantityStudents(gestionUuid);

    }

    public List<ReportDTO> getReport(String gestionUuid) {
        List<StudentDTO> studentDTOList=studentJdbcRepository.listStudents(gestionUuid);
        if (studentDTOList.isEmpty()){
            return new ArrayList<>();
        }
        List<ReportDTO> reportDTOList=new ArrayList<ReportDTO>();
        float contador=0;
        float subjectAprove=0;
        float reportResult=0;
        StudentDTO aux= studentDTOList.get(0);
        for (int i = 0; i < studentDTOList.size(); i++) {

            if(Objects.equals(aux.getName(), studentDTOList.get(i).getName())){
                contador=contador+1;
                if(studentDTOList.get(i).getQualification()>50){subjectAprove=subjectAprove+1;}
                if(i==studentDTOList.size()-1){
                    ReportDTO reportDTOAUX1 = new ReportDTO();
                    reportDTOAUX1.setName(aux.getName());
                    reportDTOAUX1.setQualification(subjectAprove/contador);
                    reportDTOList.add(reportDTOAUX1);
                }
            } else {
                ReportDTO reportDTO = new ReportDTO();
                reportDTO.setName(aux.getName());
                reportDTO.setQualification(subjectAprove/contador);
                reportDTOList.add(reportDTO);
                reportResult+=subjectAprove/contador;
                contador=1;
                if(studentDTOList.get(i).getQualification()>50){subjectAprove=1;}else{subjectAprove=0;}
                aux=studentDTOList.get(i);
                if(i==studentDTOList.size()-1){
                    ReportDTO reportDTOAUX = new ReportDTO();
                    reportDTOAUX.setName(aux.getName());
                    reportDTOAUX.setQualification(subjectAprove/contador);
                    reportDTOList.add(reportDTOAUX);
                }
            }
        }
        return reportDTOList;
    }
}
