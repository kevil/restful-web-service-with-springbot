package com.dedae.restfulwebservice.services.implement;

import com.dedae.restfulwebservice.domain.entities.Career;
import com.dedae.restfulwebservice.domain.entities.Faculty;
import com.dedae.restfulwebservice.domain.entities.Gestion;
import com.dedae.restfulwebservice.dto.CareerDTO;
import com.dedae.restfulwebservice.dto.FacultyDTO;
import com.dedae.restfulwebservice.dto.GestionDTO;
import com.dedae.restfulwebservice.exceptions.NotFoundException;
import com.dedae.restfulwebservice.repositories.data.CareerRepository;
import com.dedae.restfulwebservice.repositories.data.FacultyRepository;
import com.dedae.restfulwebservice.repositories.data.GestionRepository;
import com.dedae.restfulwebservice.services.mapper.CareerMapper;
import com.dedae.restfulwebservice.services.mapper.FacultyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class FacultyService {
    @Autowired
    private FacultyRepository facultyRepository;
    @Autowired
    GestionRepository gestionRepository;
    @Autowired
    private FacultyMapper facultyMapper;
    @Autowired
    CareerRepository careerRepository;
    @Autowired
    private CareerMapper careerMapper;
    public FacultyDTO getFaculty(String facultyUuid) {
        Faculty faculty = facultyRepository.findByUuid(facultyUuid);
        if(faculty==null){
            throw new NotFoundException("Faculty",facultyUuid);
        }
        return facultyMapper.toDTO(faculty, true);
    }

    public FacultyDTO saveFaculty(FacultyDTO facultyDTO) {
        GestionDTO gestionDTO = facultyDTO.getGestionDTO();
        if (gestionDTO == null){
            throw new NotFoundException("Gestion",null);
        }
        Gestion gestion = gestionRepository.findByUuid(gestionDTO.getUuid());
        if (gestion == null){
            throw new NotFoundException("Gestion",null);
        }
        Faculty faculty = facultyMapper.getFaculty(facultyDTO, gestion);

        facultyRepository.save(faculty);
        return facultyMapper.toDTO(faculty, true);
    }


    public FacultyDTO updateFaculty(FacultyDTO facultyDTO) {
        Faculty facultyExample1 = new Faculty(facultyDTO.getUuid());
        Optional<Faculty> optionalFaculty = facultyRepository.findOne(Example.of(facultyExample1));

        if(optionalFaculty.isEmpty()){
            throw new NotFoundException("Faculty", facultyDTO.getUuid());
        }

        Faculty faculty = optionalFaculty.get();
        faculty.setName(facultyDTO.getName());
        faculty.setCode(facultyDTO.getCode());

        facultyRepository.save(faculty);
        return facultyMapper.toDTO(faculty, true);
    }

    public FacultyDTO deleteFaculty(String facultyUuid) {
        Faculty facultyExample1 = new Faculty(facultyUuid);
        Optional<Faculty> optionalFaculty = facultyRepository.findOne(Example.of(facultyExample1));

        if(optionalFaculty.isEmpty()){
            throw new NotFoundException("Faculty", facultyUuid);
        }
        facultyRepository.delete(optionalFaculty.get());
        return facultyMapper.toDTO(optionalFaculty.get(), true);
    }

    public List<CareerDTO> getCareers(String facultyUuid) {
        Faculty facultySearch = new Faculty();
        facultySearch.setUuid(facultyUuid);
        Optional<Faculty> optionalFaculty = facultyRepository.findOne(Example.of(facultySearch));

        if(optionalFaculty.isEmpty()){
            throw new NotFoundException("Faculty", facultyUuid);
        }
        Faculty faculty = optionalFaculty.get();
        Faculty facultyExample = new Faculty();
        facultyExample.setId(faculty.getId());

        Career example = new Career();
        example.setFaculty(facultyExample);

        List<Career> careers = careerRepository.findAll(Example.of(example));
        return careers
                .stream()
                .map(career -> careerMapper.toDTO(career, false))
                .collect(Collectors.toList());
    }
}
