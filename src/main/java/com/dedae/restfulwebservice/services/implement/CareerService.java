package com.dedae.restfulwebservice.services.implement;

import com.dedae.restfulwebservice.domain.entities.Career;
import com.dedae.restfulwebservice.domain.entities.Faculty;
import com.dedae.restfulwebservice.domain.entities.Subject;
import com.dedae.restfulwebservice.dto.CareerDTO;
import com.dedae.restfulwebservice.dto.FacultyDTO;
import com.dedae.restfulwebservice.dto.SubjectDTO;
import com.dedae.restfulwebservice.exceptions.NotFoundException;
import com.dedae.restfulwebservice.repositories.data.CareerRepository;
import com.dedae.restfulwebservice.repositories.data.FacultyRepository;
import com.dedae.restfulwebservice.repositories.data.SubjectRepository;
import com.dedae.restfulwebservice.services.mapper.CareerMapper;
import com.dedae.restfulwebservice.services.mapper.FacultyMapper;
import com.dedae.restfulwebservice.services.mapper.SubjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class CareerService {
    @Autowired
    private FacultyRepository facultyRepository;
    @Autowired
    private FacultyMapper facultyMapper;
    @Autowired
    private SubjectMapper subjectMapper;
    @Autowired
    CareerRepository careerRepository;
    @Autowired
    SubjectRepository subjectRepository;
    @Autowired
    private CareerMapper careerMapper;
    public CareerDTO getCareer(String careerUuid) {
        Career career = careerRepository.findByUuid(careerUuid);
        if(career==null){
            throw new NotFoundException("Career",careerUuid);
        }
        return careerMapper.toDTO(career, true);
    }

    public CareerDTO saveCareer(CareerDTO careerDTO) {
        FacultyDTO facultyDTO = careerDTO.getFaculty();
        if (facultyDTO == null){
            throw new NotFoundException("Faculty",null);
        }
        Faculty faculty = facultyRepository.findByUuid(facultyDTO.getUuid());
        if (faculty == null){
            throw new NotFoundException("Faculty",null);
        }
        Career career = careerMapper.getCareer(careerDTO, faculty);

        careerRepository.save(career);
        return careerMapper.toDTO(career, true);
    }

    public CareerDTO updateCareer(CareerDTO careerDTO) {
        Career careerExample1 = new Career(careerDTO.getUuid());
        Optional<Career> optionalCareer = careerRepository.findOne(Example.of(careerExample1));

        if(optionalCareer.isEmpty()){
            throw new NotFoundException("Career", careerDTO.getUuid());
        }

        Career career = optionalCareer.get();
        career.setName(careerDTO.getName());
        career.setCode(careerDTO.getCode());

        careerRepository.save(career);
        return careerMapper.toDTO(career, true);
    }

    public CareerDTO deleteCareer(String careerUuid) {
        Career careerExample1 = new Career(careerUuid);
        Optional<Career> optionalCareer = careerRepository.findOne(Example.of(careerExample1));

        if(optionalCareer.isEmpty()){
            throw new NotFoundException("Career", careerUuid);
        }
        careerRepository.delete(optionalCareer.get());
        return careerMapper.toDTO(optionalCareer.get(), true);
    }

    public List<SubjectDTO> getSubjects(String careerUuid) {
        Career careerSearch = new Career();
        careerSearch.setUuid(careerUuid);
        Optional<Career> optionalCareer = careerRepository.findOne(Example.of(careerSearch));

        if(optionalCareer.isEmpty()){
            throw new NotFoundException("Career", careerUuid);
        }
        Career career = optionalCareer.get();
        Career careerExample = new Career();
        careerExample.setId(career.getId());

        Subject example = new Subject();
        example.setCareer(careerExample);

        List<Subject> subjects = subjectRepository.findAll(Example.of(example));
        return subjects
                .stream()
                .map(subject -> subjectMapper.toDTO(subject, false))
                .collect(Collectors.toList());
    }
}

