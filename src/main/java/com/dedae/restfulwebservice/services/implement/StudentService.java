package com.dedae.restfulwebservice.services.implement;

import com.dedae.restfulwebservice.domain.entities.Student;
import com.dedae.restfulwebservice.domain.entities.Subject;
import com.dedae.restfulwebservice.dto.StudentDTO;
import com.dedae.restfulwebservice.dto.SubjectDTO;
import com.dedae.restfulwebservice.exceptions.NotFoundException;
import com.dedae.restfulwebservice.repositories.data.CareerRepository;
import com.dedae.restfulwebservice.repositories.data.StudentRepository;
import com.dedae.restfulwebservice.repositories.data.SubjectRepository;
import com.dedae.restfulwebservice.services.mapper.CareerMapper;
import com.dedae.restfulwebservice.services.mapper.StudentMapper;
import com.dedae.restfulwebservice.services.mapper.SubjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class StudentService {
    @Autowired
    private SubjectMapper subjectMapper;
    @Autowired
    CareerRepository careerRepository;
    @Autowired
    SubjectRepository subjectRepository;
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    private CareerMapper careerMapper;
    @Autowired
    private StudentMapper studentMapper;
    public StudentDTO getStudent(String studentUuid) {
        Student student = studentRepository.findByUuid(studentUuid);
        if(student==null){
            throw new NotFoundException("Student",studentUuid);
        }
        return studentMapper.toDTO(student, true);
    }

    public List<StudentDTO> saveStudent(List<StudentDTO> studentListDTO) {
        SubjectDTO subjectDTO = studentListDTO.get(0).getSubject();
        if (subjectDTO == null){
            throw new NotFoundException("Subject",null);
        }
        Subject subject = subjectRepository.findByUuid(subjectDTO.getUuid());
        if (subject == null){
            throw new NotFoundException("Subject",null);
        }
        List<Student> listStudent = studentListDTO
                .stream()
                .map(studentDTO -> studentMapper.getStudent(studentDTO,subject))
                .collect(Collectors.toList());
        //Student student = studentMapper.getStudent(studentDTO, subject);

        studentRepository.saveAll(listStudent);
        return listStudent
                .stream()
                .map(student -> studentMapper.toDTO(student,true))
                .collect(Collectors.toList());
    }

    public StudentDTO updateStudent(StudentDTO studentDTO) {
        Student studentExample1 = new Student(studentDTO.getUuid());
        Optional<Student> optionalStudent = studentRepository.findOne(Example.of(studentExample1));

        if(optionalStudent.isEmpty()){
            throw new NotFoundException("Student", studentDTO.getUuid());
        }

        Student student = optionalStudent.get();
        student.setName(studentDTO.getName());
        student.setCode_sys(studentDTO.getCode_sys());
        student.setQualification(studentDTO.getQualification());
        student.setStatus(studentDTO.getStatus());

        studentRepository.save(student);
        return studentMapper.toDTO(student, true);
    }

    public void deleteStudent() {
        studentRepository.deleteAll();
    }
}
