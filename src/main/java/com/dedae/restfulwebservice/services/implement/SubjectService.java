package com.dedae.restfulwebservice.services.implement;

import com.dedae.restfulwebservice.domain.entities.Career;
import com.dedae.restfulwebservice.domain.entities.Student;
import com.dedae.restfulwebservice.domain.entities.Subject;
import com.dedae.restfulwebservice.dto.CareerDTO;
import com.dedae.restfulwebservice.dto.StudentDTO;
import com.dedae.restfulwebservice.dto.SubjectDTO;
import com.dedae.restfulwebservice.exceptions.NotFoundException;
import com.dedae.restfulwebservice.repositories.data.CareerRepository;
import com.dedae.restfulwebservice.repositories.data.StudentRepository;
import com.dedae.restfulwebservice.repositories.data.SubjectRepository;
import com.dedae.restfulwebservice.repositories.jdbc.StudentJdbcRepository;
import com.dedae.restfulwebservice.services.mapper.CareerMapper;
import com.dedae.restfulwebservice.services.mapper.StudentMapper;
import com.dedae.restfulwebservice.services.mapper.SubjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class SubjectService {

    @Autowired
    private SubjectMapper subjectMapper;
    @Autowired
    CareerRepository careerRepository;
    @Autowired
    SubjectRepository subjectRepository;
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    private CareerMapper careerMapper;
    @Autowired
    private StudentMapper studentMapper;
    private final StudentJdbcRepository studentJdbcRepository;

    public SubjectService(StudentJdbcRepository studentJdbcRepository) {
        this.studentJdbcRepository = studentJdbcRepository;
    }

    public SubjectDTO getSubject(String subjectUuid) {
        Subject subject = subjectRepository.findByUuid(subjectUuid);
        if(subject==null){
            throw new NotFoundException("Career",subjectUuid);
        }
        return subjectMapper.toDTO(subject, true);
    }

    public SubjectDTO saveSubject(SubjectDTO subjectDTO) {
        CareerDTO careerDTO = subjectDTO.getCareer();
        if (careerDTO == null){
            throw new NotFoundException("Career",null);
        }
        Career career = careerRepository.findByUuid(careerDTO.getUuid());
        if (career == null){
            throw new NotFoundException("Career",null);
        }
        Subject subject = subjectMapper.getSubject(subjectDTO, career);

        subjectRepository.save(subject);
        return subjectMapper.toDTO(subject, true);
    }

    public SubjectDTO updateSubject(SubjectDTO subjectDTO) {
        Subject subjectExample1 = new Subject(subjectDTO.getUuid());
        Optional<Subject> optionalSubject = subjectRepository.findOne(Example.of(subjectExample1));

        if(optionalSubject.isEmpty()){
            throw new NotFoundException("Subject", subjectDTO.getUuid());
        }

        Subject subject = optionalSubject.get();
        subject.setName(subjectDTO.getName());
        subject.setCode(subjectDTO.getCode());
        subject.setGroup_subject(subjectDTO.getGroup());

        subjectRepository.save(subject);
        return subjectMapper.toDTO(subject, true);
    }

    public SubjectDTO deleteSubject(String subjectUuid) {
        Subject subjectExample1 = new Subject(subjectUuid);
        Optional<Subject> optionalSubject = subjectRepository.findOne(Example.of(subjectExample1));

        if(optionalSubject.isEmpty()){
            throw new NotFoundException("Subject", subjectUuid);
        }
        subjectRepository.delete(optionalSubject.get());
        return subjectMapper.toDTO(optionalSubject.get(), true);
    }

    public List<StudentDTO> getStudents(String subjectUuid) {
        Subject subjectSearch = new Subject();
        subjectSearch.setUuid(subjectUuid);
        Optional<Subject> optionalSubject = subjectRepository.findOne(Example.of(subjectSearch));

        if(optionalSubject.isEmpty()){
            throw new NotFoundException("Subject", subjectUuid);
        }
        Subject subject = optionalSubject.get();
        Subject subjectExample = new Subject();
        subjectExample.setId(subject.getId());

        Student example = new Student();
        example.setSubject(subjectExample);

        List<Student> students = studentRepository.findAll(Example.of(example));
        return students
                .stream()
                .map(student -> studentMapper.toDTO(student, false))
                .collect(Collectors.toList());
    }

    public void deleteStudents(String subjectUuid) {
        Subject subjectExample1 = new Subject(subjectUuid);
        Optional<Subject> optionalSubject = subjectRepository.findOne(Example.of(subjectExample1));

        if(optionalSubject.isEmpty()){
            throw new NotFoundException("Subject", subjectUuid);
        }
        Subject subject = optionalSubject.get();
        studentJdbcRepository.delete(subject.getId());
    }
}
