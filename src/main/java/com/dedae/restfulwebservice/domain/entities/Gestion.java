package com.dedae.restfulwebservice.domain.entities;

import jakarta.persistence.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "gestion")
@SQLDelete(sql = "UPDATE gestion SET deleted = true WHERE id=?")
@Where(clause = "deleted = false")
public class Gestion {
    @Id
    @SequenceGenerator(name = "gestion_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gestion_sequence")
    private Integer id;
    @Column(updatable = false, nullable = false, unique = true, length = 36)
    private String uuid;
    @Column(updatable = true)
    private Integer num;
    @Column(updatable = true)
    private Integer yearG;
    @OneToMany(mappedBy = "gestion", cascade = CascadeType.REMOVE)
    private List<Faculty> facultyList;
    @Column(columnDefinition = "BOOLEAN NOT NULL DEFAULT '0'")
    private boolean deleted;
    public Gestion() {
    }
    public Gestion(Integer id) {
        this.id = id;
    }
    public Gestion(String uuid) {
        this.uuid = uuid;
    }
    public Gestion(Integer id, String uuid, Integer num, Integer yearG) {
        this.id = id;
        this.uuid = uuid;
        this.num = num;
        this.yearG = yearG;
    }

    public Integer getYearG() {
        return yearG;
    }

    public void setYearG(Integer yearG) {
        this.yearG = yearG;
    }

    public List<Faculty> getFacultyList() {
        return facultyList;
    }

    public void setFacultyList(List<Faculty> facultyList) {
        this.facultyList = facultyList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getYear() {
        return yearG;
    }

    public void setYear(Integer yearG) {
        this.yearG = yearG;
    }


    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
    @PrePersist
    public void initializeUuid(){
        this.setUuid(UUID.randomUUID().toString());
    }
}