package com.dedae.restfulwebservice.domain.entities;

import jakarta.persistence.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "subject")
@SQLDelete(sql = "UPDATE subject SET deleted = true WHERE id=?")
@Where(clause = "deleted = false")
public class Subject {
    @Id
    @SequenceGenerator(name = "subject_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "subject_sequence")
    private Integer id;
    @Column(updatable = false, nullable = false, unique = true, length = 36)
    private String uuid;
    @Column(updatable = true,length = 20)
    private String code;
    @Column(updatable = true,length = 500)
    private String name;
    @Column(updatable = true)
    private Integer group_subject;
    @ManyToOne(fetch = FetchType.LAZY)
    private Career career;
    @OneToMany(mappedBy = "subject", cascade = CascadeType.REMOVE)
    private List<Student> studentsList;
    @Column(columnDefinition = "BOOLEAN NOT NULL DEFAULT '0'")
    private boolean deleted;

    public Subject() {
    }

    public Subject(Integer id, String uuid, String code, String name, Integer group_subject) {
        this.id = id;
        this.uuid = uuid;
        this.code = code;
        this.name = name;
        this.group_subject = group_subject;
    }

    public Subject(String uuid) {
        this.uuid = uuid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGroup_subject() {
        return group_subject;
    }

    public void setGroup_subject(Integer group_subject) {
        this.group_subject = group_subject;
    }

    public Career getCareer() {
        return career;
    }

    public void setCareer(Career career) {
        this.career = career;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<Student> getStudentsList() {
        return studentsList;
    }

    public void setStudentsList(List<Student> studentsList) {
        this.studentsList = studentsList;
    }
    @PrePersist
    public void initializeUuid(){
        this.setUuid(UUID.randomUUID().toString());
    }
}
