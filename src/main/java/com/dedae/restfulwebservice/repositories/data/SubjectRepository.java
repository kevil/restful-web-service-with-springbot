package com.dedae.restfulwebservice.repositories.data;

import com.dedae.restfulwebservice.domain.entities.Career;
import com.dedae.restfulwebservice.domain.entities.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.util.List;

public interface SubjectRepository extends JpaRepository<Subject, Integer> {
    @Query("SELECT s FROM Subject s Where s.uuid=?1")
    Subject findByUuid(String uuid);

    @Query("SELECT s FROM Subject s Where s.career=?1")
    List<Subject> getByCareerId(Career career);
}