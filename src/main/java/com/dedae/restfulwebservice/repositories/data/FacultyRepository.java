package com.dedae.restfulwebservice.repositories.data;

import com.dedae.restfulwebservice.domain.entities.Faculty;
import com.dedae.restfulwebservice.domain.entities.Gestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.util.List;


public interface FacultyRepository extends JpaRepository<Faculty, Integer> {
    @Query("SELECT f FROM Faculty f Where f.uuid=?1")
    Faculty findByUuid(String uuid);

    @Query("SELECT f FROM Faculty f Where f.gestion=?1")
    List<Faculty> getByGestionId(Gestion gestion);
}
