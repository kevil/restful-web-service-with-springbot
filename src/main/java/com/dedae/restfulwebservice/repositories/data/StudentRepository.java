package com.dedae.restfulwebservice.repositories.data;

import com.dedae.restfulwebservice.domain.entities.Student;
import com.dedae.restfulwebservice.domain.entities.Subject;
import org.hibernate.annotations.SQLDelete;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.util.List;
import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student, Integer> {
    @Query("SELECT s FROM Student s Where s.uuid=?1")
    Student findByUuid(String uuid);

    @Query("SELECT s FROM Student s Where s.subject=?1")
    List<Student> getByCareerId(Subject subject);

    @Query("DELETE FROM Student s Where s.subject=?1")
    void deleteAllBySubject(Subject subject);
}