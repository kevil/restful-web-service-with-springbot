package com.dedae.restfulwebservice.repositories.data;

import com.dedae.restfulwebservice.domain.entities.Gestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface GestionRepository extends JpaRepository<Gestion, Integer> {
    @Query("SELECT g FROM Gestion g Where g.uuid=?1")
    Gestion findByUuid(String uuid);

}
