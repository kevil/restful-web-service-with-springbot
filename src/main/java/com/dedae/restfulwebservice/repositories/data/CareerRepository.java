package com.dedae.restfulwebservice.repositories.data;

import com.dedae.restfulwebservice.domain.entities.Career;
import com.dedae.restfulwebservice.domain.entities.Faculty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.util.List;

public interface CareerRepository extends JpaRepository<Career, Integer> {
    @Query("SELECT c FROM Career c Where c.uuid=?1")
    Career findByUuid(String uuid);

    @Query("SELECT c FROM Career c Where c.faculty=?1")
    List<Career> getByGestionId(Faculty faculty);
}