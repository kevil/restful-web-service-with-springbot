package com.dedae.restfulwebservice.repositories.jdbc.rowmapper;

import com.dedae.restfulwebservice.dto.StudentDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReportRowMapper implements RowMapper<StudentDTO>{
        @Override
        public StudentDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new StudentDTO(
                    rs.getString("name"),
                    rs.getInt("qualification")
            );
        }
}
