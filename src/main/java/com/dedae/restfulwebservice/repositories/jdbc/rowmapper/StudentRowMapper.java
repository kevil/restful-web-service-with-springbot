package com.dedae.restfulwebservice.repositories.jdbc.rowmapper;

import com.dedae.restfulwebservice.dto.StudentDTO;
import com.dedae.restfulwebservice.dto.SubjectDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentRowMapper implements RowMapper<StudentDTO> {
    @Override
    public StudentDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new StudentDTO(
                rs.getString("name"),
                rs.getString("code_sys")
        );
    }
}
