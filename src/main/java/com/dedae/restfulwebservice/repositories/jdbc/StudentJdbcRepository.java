package com.dedae.restfulwebservice.repositories.jdbc;

import com.dedae.restfulwebservice.domain.entities.Subject;
import com.dedae.restfulwebservice.dto.StudentDTO;
import com.dedae.restfulwebservice.repositories.jdbc.rowmapper.ReportRowMapper;
import com.dedae.restfulwebservice.repositories.jdbc.rowmapper.StudentRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StudentJdbcRepository {
    private final JdbcTemplate jdbcTemplate;

    public StudentJdbcRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    public void delete(Integer subjectId) {
        var sql = """
                DELETE FROM student WHERE subject_id=?
                """;
        jdbcTemplate.update(sql, subjectId);
    }
    public List<StudentDTO> quantityStudents(String gestionUuid) {
        var sql = """
                SELECT DISTINCT student.name, student.code_sys
                FROM student
                INNER JOIN subject ON student.subject_id = subject.id
                INNER JOIN career ON subject.career_id = career.id
                INNER JOIN faculty ON career.faculty_id = faculty.id
                INNER JOIN gestion ON faculty.gestion_id = gestion.id
                WHERE gestion.uuid = ? AND student.deleted = false
                """;
        return jdbcTemplate.query(sql,new StudentRowMapper(), gestionUuid);
    }
    public List<StudentDTO> listStudents(String gestionUuid) {
        var sql = """
                SELECT student.name , student.qualification
                FROM student
                INNER JOIN subject ON student.subject_id = subject.id
                INNER JOIN career ON subject.career_id = career.id
                INNER JOIN faculty ON career.faculty_id = faculty.id
                INNER JOIN gestion ON faculty.gestion_id = gestion.id
                WHERE gestion.uuid = ? AND student.deleted = false
                ORDER BY student.name
                """;
        return jdbcTemplate.query(sql,new ReportRowMapper(), gestionUuid);
    }
}
